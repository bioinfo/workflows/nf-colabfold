# nf-colabfold: Singularity recipes

In this directory, you can find the singularity recipe allowing you to create manually dependencies necessary for nf-colabfold pipeline. 

The colabfold_receipe.def singularity recipe is a slightly modified version adapted from [colabfold_singularity](https://github.com/tubiana/colabfold_singularity) to enable image creation on Ifremer cluster.

You can get a pre-built Singularity image of Colabfold from [this repository](https://data-dataref.ifremer.fr/bioinfo/ifremer/sebimer/tools/nf-colabfold/). 
