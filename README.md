# **nf-colabfold: a Nextflow pipeline to run Colabfold on mixed CPU-GPU clusters**.

[![Nextflow](https://img.shields.io/badge/nextflow-%E2%89%A520.10.0-23aa62.svg?labelColor=000000)](https://www.nextflow.io/)
[![Run with with singularity](https://img.shields.io/badge/run%20with-singularity-1d355c.svg?labelColor=000000)](https://sylabs.io/docs/)
[![Developers](https://img.shields.io/badge/Developers-SeBiMER-yellow?labelColor=000000)](https://ifremer-bioinformatics.github.io/)

## Introduction

This is a simple two steps pipeline enabling to automate the use of Colabfold on a mixed CPU/GPU cluster. MMSEQS2 will be executed on standard multi-CPUs computing nodes, whereas Alphafold will be executed on a GPU node.

It has been primarily designed to work on DATARMOR supercomputer at Ifremer, using PBS pro job scheduler. You may have to update configutation files of Nextflow to run the pipeline on your cluster (see section below).

## Requirements

### Singularity image 

Get pre-built Singularity image of Colabfold from [this repository](https://data-dataref.ifremer.fr/bioinfo/ifremer/sebimer/tools/nf-colabfold/). This image has been created by nf-colabfold authors from Singularity recipe provided in containers folder of [this project](https://github.com/tubiana/colabfold_singularity).

Place that image in "containers" sub-folder of this project. The use of that image is stated in nextflow.config configuration file, see below.
 
### Reference bank

Get "uniref30_2103.tar.gz" data set from [Colabfold](https://colabfold.mmseqs.com/) and index the data file as explained by Colabfold authors using mmseqs. You may look at [setup_databases.sh script](https://github.com/sokrypton/ColabFold/blob/main/setup_databases.sh) to do that step.

### Alphafold params

Get Alphafold parameters using [download_alphafold_params.sh script](https://github.com/deepmind/alphafold/blob/main/scripts/download_alphafold_params.sh).

## Configuration

Before using the pipeline, you will have to setup the following Nextflow configuration files in order to allow nf-colabfold to work on your cluster:

To do once:

- nextflow.config: locate directives "singularity.runOptions" and update path to reference bank and Alphafold parameters as they are installed during previous step (see Requirements);
- conf/resources.config: update resources to match your cluster config.

To do for each analysis:

- conf/base.config: update "projectName", as well as "fasta" and modelType" variables. 

## Credits

nf-colabfold is written by [SeBiMER](https://ifremer-bioinformatics.github.io/), the Bioinformatics Core Facility of [IFREMER](https://wwz.ifremer.fr/en/).

## Contributions

We welcome contributions to the pipeline. If such case you can do one of the following:
* Use issues to submit your questions 
* Fork the project, do your developments and submit a pull request
* Contact us (see email below) 

## Support

For further information or help, don't hesitate to get in touch with the developpers: 

![sebimer email](assets/sebimer-email.png)

