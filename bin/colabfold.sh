#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: run Colabfold
##                                                                           ##
###############################################################################

# var settings
args=("$@")
MODEL_TYPE=${args[0]}
MSA_DIR=${args[1]}
OUT_DIR=${args[2]}
LOGCMD=${args[3]}

mkdir -p $OUT_DIR
CMD="colabfold_batch --model-type ${MODEL_TYPE} ${MSA_DIR} ${OUT_DIR}"
echo ${CMD} > ${LOGCMD}
eval ${CMD}

