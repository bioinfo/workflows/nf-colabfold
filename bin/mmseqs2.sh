#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: run MMSEQS2 through colabfold_search                   ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
CPUS=${args[0]}
REF_DB_PATH=${args[1]}
REF_DB_NAME=${args[2]}
FASTA=${args[3]}
OUTDIR=${args[4]}
LOGCMD=${args[5]}

mkdir -p $OUTDIR

CMD="colabfold_search -s 8 \
            --db1 $REF_DB_NAME \
            --use-env 0 \
            --use-templates 0 \
            --filter 1 \
            --mmseqs /opt/mmseqs/bin/mmseqs \
            --expand-eval inf \
            --align-eval 10 \
            --diff 0 \
            --qsc -20.0 \
            --max-accept 10 \
            --db-load-mode 2 \
            --thread ${CPUS} \
            ${FASTA} ${REF_DB_PATH} ${OUTDIR}"
echo ${CMD} > ${LOGCMD}
eval ${CMD}

