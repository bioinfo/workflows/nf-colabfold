process mmseqs2_t {

    label 'highRAM_omp'
 
    publishDir "${params.dirname_msa}", mode: 'copy', pattern: 'msa_outdir/*.a3m'
    publishDir "${params.dirname_msa}", mode: 'copy', pattern: 'mmseqs2_t.*'

    input:
        path(input_fasta)

    output:
        path('msa_outdir/0.a3m'), emit: msa
        path("mmseqs2_t.*")

    script:
    """
    mmseqs2.sh ${task.cpus} /alphafold/database/${params.dbname} ${params.dbname}_db ${input_fasta} msa_outdir mmseqs2_t.cmd &> mmseqs2_t.log 2>&1
    """
}
