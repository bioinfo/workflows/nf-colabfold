process colabfold_t {

    label 'stdRAM_gpu'
    beforeScript '. /etc/profile.d/modules.sh ; module load singularity/3.4.1 ; ulimit -u 16384 ; ulimit -v unlimited ; CUDA_VISIBLE_DEVICES=0'

    publishDir "${params.predictions}", mode: 'copy', pattern: 'predictions_outdir/*.pdb'
    publishDir "${params.predictions}", mode: 'copy', pattern: 'predictions_outdir/*.json'
    publishDir "${params.predictions}", mode: 'copy', pattern: 'predictions_outdir/*.png'
    publishDir "${params.predictions}", mode: 'copy', pattern: 'colabfold_t.*'

    input:
        path(mmseq_msa)

    output:
        path('predictions_outdir/*.pdb')
        path('predictions_outdir/*.json')
        path('predictions_outdir/*.png')
        path("colabfold_t.*")

    script:
    """
    colabfold.sh ${params.modelType} ${mmseq_msa} predictions_outdir colabfold_t.cmd &> colabfold_t.log 2>&1
    """
}
